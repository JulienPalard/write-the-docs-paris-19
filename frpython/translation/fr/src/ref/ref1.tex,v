head	1.2;
access;
symbols
	import-release_2_0_1:1.1.1.1
	IMPORT:1.1.1
	import-release20:1.1.1.1
	import-r20c1:1.1.1.1
	Import_Dist_Orig:1.1.1;
locks; strict;
comment	@% @;


1.2
date	2001.01.21.04.22.54;	author blacherez;	state Exp;
branches;
next	1.1;

1.1
date	2000.10.17.06.30.22;	author olberger;	state Exp;
branches
	1.1.1.1;
next	;

1.1.1.1
date	2000.10.17.06.30.22;	author olberger;	state Exp;
branches;
next	;


desc
@@


1.2
log
@Premi�re traduction
@
text
@% Texte original ttir� de la documentation de r�f�rence officielle dat�e de la fa�on suivante  : 
% October 16, 2000 Release 2.0

%\chapter{Introduction\label{introduction}}
%
%This reference manual describes the Python programming language.
%It is not intended as a tutorial.
%
%While I am trying to be as precise as possible, I chose to use English
%rather than formal specifications for everything except syntax and
%lexical analysis.  This should make the document more understandable
%to the average reader, but will leave room for ambiguities.
%Consequently, if you were coming from Mars and tried to re-implement
%Python from this document alone, you might have to guess things and in
%fact you would probably end up implementing quite a different language.
%On the other hand, if you are using
%Python and wonder what the precise rules about a particular area of
%the language are, you should definitely be able to find them here.
%If you would like to see a more formal definition of the language,
%maybe you could volunteer your time --- or invent a cloning machine
%:-).
%
%It is dangerous to add too many implementation details to a language
%reference document --- the implementation may change, and other
%implementations of the same language may work differently.  On the
%other hand, there is currently only one Python implementation in
%widespread use (although a second one now exists!), and
%its particular quirks are sometimes worth being mentioned, especially
%where the implementation imposes additional limitations.  Therefore,
%you'll find short ``implementation notes'' sprinkled throughout the
%text.
%
%Every Python implementation comes with a number of built-in and
%standard modules.  These are not documented here, but in the separate
%\citetitle[../lib/lib.html]{Python Library Reference} document.  A few
%built-in modules are mentioned when they interact in a significant way
%with the language definition.
%
%\section{Notation\label{notation}}
%
%The descriptions of lexical analysis and syntax use a modified BNF
%grammar notation.  This uses the following style of definition:
%\index{BNF}
%\index{grammar}
%\index{syntax}
%\index{notation}
%
%\begin{verbatim}
%name:           lc_letter (lc_letter | "_")*
%lc_letter:      "a"..."z"
%\end{verbatim}
%
%The first line says that a \code{name} is an \code{lc_letter} followed by
%a sequence of zero or more \code{lc_letter}s and underscores.  An
%\code{lc_letter} in turn is any of the single characters \character{a}
%through \character{z}.  (This rule is actually adhered to for the
%names defined in lexical and grammar rules in this document.)
%
%Each rule begins with a name (which is the name defined by the rule)
%and a colon.  A vertical bar (\code{|}) is used to separate
%alternatives; it is the least binding operator in this notation.  A
%star (\code{*}) means zero or more repetitions of the preceding item;
%likewise, a plus (\code{+}) means one or more repetitions, and a
%phrase enclosed in square brackets (\code{[ ]}) means zero or one
%occurrences (in other words, the enclosed phrase is optional).  The
%\code{*} and \code{+} operators bind as tightly as possible;
%parentheses are used for grouping.  Literal strings are enclosed in
%quotes.  White space is only meaningful to separate tokens.
%Rules are normally contained on a single line; rules with many
%alternatives may be formatted alternatively with each line after the
%first beginning with a vertical bar.
%
%In lexical definitions (as the example above), two more conventions
%are used: Two literal characters separated by three dots mean a choice
%of any single character in the given (inclusive) range of \ASCII{}
%characters.  A phrase between angular brackets (\code{<...>}) gives an
%informal description of the symbol defined; e.g., this could be used
%to describe the notion of `control character' if needed.
%\index{lexical definitions}
%\index{ASCII@@\ASCII{}}
%
%Even though the notation used is almost the same, there is a big
%difference between the meaning of lexical and syntactic definitions:
%a lexical definition operates on the individual characters of the
%input source, while a syntax definition operates on the stream of
%tokens generated by the lexical analysis.  All uses of BNF in the next
%chapter (``Lexical Analysis'') are lexical definitions; uses in
%subsequent chapters are syntactic definitions.

\chapter{Introduction\label{introduction}}

Ce manuel de r�f�rence d�crit le language de programmation Python. Ce n'est
pas un tutorial.

Afin de rester le plus pr�cis possible, j'ai choisi d'utiliser l'anglais plutot
que des sp�cifications formelles except� pour d�crire la syntaxe et l'analyse
syntaxique. Cela devrait rendre le document plus compr�hensible pour le lecteur
moyen, mais laisse un peu de place pour des ambiguit�s. En cons�quent, si vous
veniez de mars et essayiez de r�impl�menter Python � partir de ce seul document,
vous devriez deviner certaines choses et, en fait, vous finiriez par impl�menter
un langage sensiblement diff�rent. D'un autre cot�, si vous utilisez Python
et que vous vous demandez quelles sont les r�gles pr�cises qui r�gissent une
zone particuli�re du langage, vous devriez etre capable de les trouver ici.
Si vous d�sirez voir une d�finition plus formelle du langage, peut-etre pourriez
vous donner un peu de votre temps en tant que volontaire, ou inventer une machine
de clonage :-).

Il est dangereux d'ajouter de trop nombreux d�tails d'impl�mentation dans un
document de r�f�rence ; l'impl�mentation peut changer, et d'autres impl�mentations
du meme langage peuvent fonctionner diff�remment. D'un autre cot�, il n'y a
qu'une seule impl�mentation de Python qui soit largement utilis�e (bien qu'une
seconde existe maintenant !), et il est parfois utile de d�crire ses rouages,
particuli�rement lorsque l'impl�mentation implique des limitations suppl�mentaires.
Par cons�quent, vous trouverez de courtes {}``notes d'impl�mentation{}'' dispers�es
tout au long du texte. 

Un certains nombres de modules standards sont int�gr�s � chaque impl�mentation
de Python. Ils ne sont pas document�s ici, mais dans le document s�par� 
\citetitle[../lib/lib.html]{R�f�rencedes Biblioth�ques de fonctions de Python}. Quelques un 
des modules int�gr�s sont mentionn�s lorsqu'ils int�ragissent de fa�on significative avec la d�finition
du langage.

\section{Notation\label{notation}}

Les descriptions de l'analyse syntaxique utilisent une notation grammaticale
BNF (Backus-Naur Form) modifi�e. Ceci utilise le style de d�finition suivant
:
\index{BNF}
\index{grammaire}
\index{syntaxe}
\index{notation}

\begin{verbatim}
nom:           lettre\_min~(lettre\_min~|~{}``\_{}''){*}
lettre\_min:   ``a''...''z''
\end{verbatim}

La premi�re ligne signifie qu'un \code{nom} est une \code{lettre\_min} suivie
par une suite de zero ou plus \code{lettre\_min} ou caract�re \character{_}. Une \code{lettre\_min}
correspond � n'importe quel caract�re allant de \character{a} � \character{z} (cette
r�gle est en fait valable pour les noms d�finis dans les r�gles grammaticales
et lexicales de ce document).

Chaque r�gle d�bute par un nom (qui est le nom d�fini par la r�gle) et un caract�re
{}``:{}``. Une barre verticale (\code{|}) est utilis�e pour s�parer les choix possibles;
c'est l'op�rateur le moins prioritaire dans cette notation. une �toile signifie
aucune ou plusieurs r�p�titions de l'objet pr�c�dent; de la meme fa�on, un plus
(\code{+}) correspond � une ou plusieurs r�p�titions, et une phrase d�limit�e par des
crochets (\code{[ ]}) signifie aucune ou une seule occurence (en d'autres termes,
la phrase d�limit�e est optionelle). L'op�rateur \code{*} et \code{+} sont aussi prioritaires
que possible; les parenth�ses sont utilis�es pour grouper. Les chaines de caract�res
litt�rales sont d�limit�es par des apostrophes. L'espace est seulement utile
pour s�parer les lex�mes. Les r�gles tiennent g�n�ralement sur une seule ligne;
les r�gles poss�dant de nombreux choix possible peuvent etre formatt�es diff�remment
en d�butant chaque ligne suivant la premi�re par une barre verticale.

Dans les d�finitions lexicales (comme pour l'exemple ci-dessus), deux conventions
de plus sont utilis�es : Deux caract�res litt�raux s�par�s par trois points signifie
le choix de n'importe quel caract�re dans l'intervalle (inclusif) donn� de caract�res
\ASCII{}. Une phrase d�limit�e par des signes {}``strictement inf�rieur �{}''
et {}``strictement sup�rieur �{}'' (<...>) fournit une description informelle
du symbole d�fini; par exemple, cela pourrait etre utilis� au besoin pour d�crire
la notion de {}``touche control{}''.
\index{d�finitions lexicales}
\index{ASCII@@ASCII{}}


Meme si la notation utilis�e est quasiment la meme, il y a une grosse diff�rence
entre la signification des d�finitions lexicales et syntaxiques : une d�finition
lexicale agit sur le caract�re individuel de la source d'entr�e, tandis qu'une
d�finition syntaxique agit sur le flux de l�x�mes g�n�r� par l'analyse syntaxique.
Toutes les utilisations de la norme de Backus-Naur (norme BNF) dans le prochain
chapitre ({}``Analyse Syntaxique) sont des d�finitions lexicales; cette norme
est utilis�e dans les autres chapitres pour des d�finitions syntaxiques.

@


1.1
log
@Initial revision
@
text
@d1 89
d92 2
a93 2
This reference manual describes the Python programming language.
It is not intended as a tutorial.
d95 27
a121 29
While I am trying to be as precise as possible, I chose to use English
rather than formal specifications for everything except syntax and
lexical analysis.  This should make the document more understandable
to the average reader, but will leave room for ambiguities.
Consequently, if you were coming from Mars and tried to re-implement
Python from this document alone, you might have to guess things and in
fact you would probably end up implementing quite a different language.
On the other hand, if you are using
Python and wonder what the precise rules about a particular area of
the language are, you should definitely be able to find them here.
If you would like to see a more formal definition of the language,
maybe you could volunteer your time --- or invent a cloning machine
:-).

It is dangerous to add too many implementation details to a language
reference document --- the implementation may change, and other
implementations of the same language may work differently.  On the
other hand, there is currently only one Python implementation in
widespread use (although a second one now exists!), and
its particular quirks are sometimes worth being mentioned, especially
where the implementation imposes additional limitations.  Therefore,
you'll find short ``implementation notes'' sprinkled throughout the
text.

Every Python implementation comes with a number of built-in and
standard modules.  These are not documented here, but in the separate
\citetitle[../lib/lib.html]{Python Library Reference} document.  A few
built-in modules are mentioned when they interact in a significant way
with the language definition.
d125 3
a127 2
The descriptions of lexical analysis and syntax use a modified BNF
grammar notation.  This uses the following style of definition:
d129 2
a130 2
\index{grammar}
\index{syntax}
d134 2
a135 2
name:           lc_letter (lc_letter | "_")*
lc_letter:      "a"..."z"
d138 38
a175 36
The first line says that a \code{name} is an \code{lc_letter} followed by
a sequence of zero or more \code{lc_letter}s and underscores.  An
\code{lc_letter} in turn is any of the single characters \character{a}
through \character{z}.  (This rule is actually adhered to for the
names defined in lexical and grammar rules in this document.)

Each rule begins with a name (which is the name defined by the rule)
and a colon.  A vertical bar (\code{|}) is used to separate
alternatives; it is the least binding operator in this notation.  A
star (\code{*}) means zero or more repetitions of the preceding item;
likewise, a plus (\code{+}) means one or more repetitions, and a
phrase enclosed in square brackets (\code{[ ]}) means zero or one
occurrences (in other words, the enclosed phrase is optional).  The
\code{*} and \code{+} operators bind as tightly as possible;
parentheses are used for grouping.  Literal strings are enclosed in
quotes.  White space is only meaningful to separate tokens.
Rules are normally contained on a single line; rules with many
alternatives may be formatted alternatively with each line after the
first beginning with a vertical bar.

In lexical definitions (as the example above), two more conventions
are used: Two literal characters separated by three dots mean a choice
of any single character in the given (inclusive) range of \ASCII{}
characters.  A phrase between angular brackets (\code{<...>}) gives an
informal description of the symbol defined; e.g., this could be used
to describe the notion of `control character' if needed.
\index{lexical definitions}
\index{ASCII@@\ASCII{}}

Even though the notation used is almost the same, there is a big
difference between the meaning of lexical and syntactic definitions:
a lexical definition operates on the individual characters of the
input source, while a syntax definition operates on the stream of
tokens generated by the lexical analysis.  All uses of BNF in the next
chapter (``Lexical Analysis'') are lexical definitions; uses in
subsequent chapters are syntactic definitions.
@


1.1.1.1
log
@Import version initiale de doc Python 2.0c1
@
text
@@
