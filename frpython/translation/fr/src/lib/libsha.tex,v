head	1.3;
access;
symbols
	import-release_2_0_1:1.1.1.1
	IMPORT:1.1.1
	import-release20:1.1.1.1
	import-r20c1:1.1.1.1
	Import_Dist_Orig:1.1.1;
locks; strict;
comment	@% @;


1.3
date	2000.11.13.02.06.17;	author blacherez;	state Exp;
branches;
next	1.2;

1.2
date	2000.11.13.01.52.32;	author blacherez;	state Exp;
branches;
next	1.1;

1.1
date	2000.10.17.06.29.49;	author olberger;	state Exp;
branches
	1.1.1.1;
next	;

1.1.1.1
date	2000.10.17.06.29.49;	author olberger;	state Exp;
branches;
next	;


desc
@@


1.3
log
@Modification de la pr�sentation conform�ment aux r�gles du groupe de travail
@
text
@TRANSLATION INFO :%T%% Original : release 2.0
%T%% Translation : $Id: prepare_tex.py,v 1.3.2.1 2000/10/31 22:48:10 olberger Exp $
%T%% Translator : "Pierre Quentel"

%T%% TRANSLATION ORIG%O%%\section{\module{sha} ---
%O%%         SHA message digest algorithm}
%T%% TRANSLATION %T%% FR

\section{\module{sha} ---
         Algorithme de condens� de message SHA}

%T%% /TRANSLATION
%T%% TRANSLATION ORIG%O%%\declaremodule{builtin}{sha}
%O%%\modulesynopsis{NIST's secure hash algorithm, SHA.}
%O%%\sectionauthor{Fred L. Drake, Jr.}{fdrake@@acm.org}
%T%% TRANSLATION %T%% FR

\declaremodule{builtin}{sha}
\modulesynopsis{SHA, l'algorithme s�curis� de hachage du NIST.}
\sectionauthor{Fred L. Drake, Jr.}{fdrake@@acm.org}

%T%% /TRANSLATION

%T%% TRANSLATION ORIG%O%%This module implements the interface to NIST's\index{NIST} secure hash 
%O%%algorithm,\index{Secure Hash Algorithm} known as SHA.  It is used in
%O%%the same way as the \refmodule{md5} module:\ use \function{new()}
%O%%to create an sha object, then feed this object with arbitrary strings
%O%%using the \method{update()} method, and at any point you can ask it
%O%%for the \dfn{digest} of the concatenation of the strings fed to it
%O%%so far.\index{checksum!SHA}  SHA digests are 160 bits instead of
%O%%MD5's 128 bits.
%T%% TRANSLATION %T%% FR

Ce module impl�mente l'interface vers l'algorithme s�curis� de hachage
du NIST\index{NIST}, connu sous le nom de \index{Secure Hash Algorithm}
SHA. On l'utilise de la m�me fa�on que le module \refmodule{md5} :\ 
utilisez \function{new()} pour cr�er un objet sha, puis alimentez cet
objet avec des cha�nes de caract�res arbitraires en utilisant la
m�thode \method{update()}, et � chaque instant vous pouvez lui demander
le \dfn{condens�} de la concat�nation des cha�nes de caract�res fournies
jusque l�.\index{somme de contr�le!SHA}  Les condens�s SHA sont longs de 160 bits,
au lieu de 128 pour MD5.

%T%% /TRANSLATION

%T%% TRANSLATION ORIG%O%%\begin{funcdesc}{new}{\optional{string}}
%O%%  Return a new sha object.  If \var{string} is present, the method
%O%%  call \code{update(\var{string})} is made.
%O%%\end{funcdesc}
%T%% TRANSLATION %T%% FR

\begin{funcdesc}{new}{\optional{chaine}}
  Retourne un nouvel objet sha. Si \var{chaine} est pr�sent, l'appel �
  la m�thode \code{update(\var{chaine})} est effectu�
\end{funcdesc}

%T%% /TRANSLATION

%T%% TRANSLATION ORIG%O%%The following values are provided as constants in the module and as
%O%%attributes of the sha objects returned by \function{new()}:
%T%% TRANSLATION %T%% FR

Les valeurs suivantes sont fournies comme constantes dans le module
et comme attributs des objets sha retourn�s par \function{new()}:

%T%% /TRANSLATION
%T%% TRANSLATION ORIG%O%%\begin{datadesc}{blocksize}
%O%%  Size of the blocks fed into the hash function; this is always
%O%%  \code{1}.  This size is used to allow an arbitrary string to be
%O%%  hashed.
%O%%\end{datadesc}
%T%% TRANSLATION %T%% FR

\begin{datadesc}{blocksize}
  Taille des blocs fournis � la fonction de hachage ; vaut toujours
  \code{1}.  Cette taille est utilis�e pour permettre le hachage d'une
  cha�ne de caract�res arbitraire
\end{datadesc}

%T%% /TRANSLATION
%T%% TRANSLATION ORIG%O%%\begin{datadesc}{digestsize}
%O%%  The size of the resulting digest in bytes.  This is always
%O%%  \code{20}.
%O%%\end{datadesc}
%T%% TRANSLATION %T%% FR

\begin{datadesc}{digestsize}
  La taille du condens� en octets. Vaut toujours \code{20}.
\end{datadesc}

%T%% /TRANSLATION

%T%% TRANSLATION ORIG%O%%An sha object has the same methods as md5 objects:
%T%% TRANSLATION %T%% FR

Un objet sha a les m�mes m�thodes que les objets md5 :

%T%% /TRANSLATION
%T%% TRANSLATION ORIG%O%%\begin{methoddesc}[sha]{update}{arg}
%O%%Update the sha object with the string \var{arg}.  Repeated calls are
%O%%equivalent to a single call with the concatenation of all the
%O%%arguments, i.e.\ \code{m.update(a); m.update(b)} is equivalent to
%O%%\code{m.update(a+b)}.
%O%%\end{methoddesc}
%T%% TRANSLATION %T%% FR

\begin{methoddesc}[sha]{update}{arg}
Met � jour l'objet sha avec la cha�ne de caract�res \var{arg}.  
Des appels r�p�t�s sont �quivalents � un appel unique avec la
concat�nation de tous les arguments, c'es-�-dire que 
\ \code{m.update(a); m.update(b)} est �quivalent �
\code{m.update(a+b)}.
\end{methoddesc}

%T%% /TRANSLATION
%T%% TRANSLATION ORIG%O%%\begin{methoddesc}[sha]{digest}{}
%O%%Return the digest of the strings passed to the \method{update()}
%O%%method so far.  This is a 20-byte string which may contain
%O%%non-\ASCII{} characters, including null bytes.
%O%%\end{methoddesc}
%T%% TRANSLATION %T%% FR

\begin{methoddesc}[sha]{digest}{}
Retourne le condens� des cha�nes de caract�res pass�es � la
m�thode \method{update()} jusque l�. Il s'agit d'une cha�ne de
caract�res de 20 octets qui peut contenir des caract�res non ASCII{}, y
compris des octets � z�ro.
\end{methoddesc}

%T%% /TRANSLATION
%T%% TRANSLATION ORIG%O%%\begin{methoddesc}[sha]{hexdigest}{}
%O%%Like \method{digest()} except the digest is returned as a string of
%O%%length 40, containing only hexadecimal digits.  This may 
%O%%be used to exchange the value safely in email or other non-binary
%O%%environments.
%O%%\end{methoddesc}
%T%% TRANSLATION %T%% FR

\begin{methoddesc}[sha]{hexdigest}{}
Comme \method{digest()} sauf que le condens� est retourn� sous forme
d'une cha�ne de caract�res de longueur 40, ne contenant que des
chiffres hexad�cimaux. Ceci peut �tre utilis� pour �changer des
valeurs de fa�on s�re par e-mail ou dans d'autres environnements
non binaires.
\end{methoddesc}

%T%% /TRANSLATION
%T%% TRANSLATION ORIG%O%%\begin{methoddesc}[sha]{copy}{}
%O%%Return a copy (``clone'') of the sha object.  This can be used to
%O%%efficiently compute the digests of strings that share a common initial
%O%%substring.
%O%%\end{methoddesc}
%T%% TRANSLATION %T%% FR

\begin{methoddesc}[sha]{copy}{}
Retourne une copie (``clone'') de l'objet sha. Ceci peut �tre utilis�
pour calculer efficacement les condens�s de cha�nes de caract�res qui
commencent par la m�me sous-cha�ne.
\end{methoddesc}

%T%% /TRANSLATION
%T%% TRANSLATION ORIG%O%%\begin{seealso}
%O%%  \seetitle[http://csrc.nist.gov/fips/fip180-1.txt]{Secure Hash Standard}{
%O%%            The Secure Hash Algorithm is defined by NIST document FIPS
%O%%            PUB 180-1:
%O%%            \citetitle[http://csrc.nist.gov/fips/fip180-1.txt]{Secure
%O%%            Hash Standard}, published in April of 1995.  It is
%O%%            available online as plain text (at least one diagram was
%O%%            omitted) and as PDF at
%O%%            \url{http://csrc.nist.gov/fips/fip180-1.pdf}.}
%O%%\end{seealso}
%T%% TRANSLATION %T%% FR

\begin{seealso}
  \seetitle[http://csrc.nist.gov/fips/fip180-1.txt]{Secure Hash Standard}{
            L'Algorithme S�curis� de Hachage (Secure Hash Algorithm) est
            d�fini par le document du NIST FIPS PUB 180-1:
            \citetitle[http://csrc.nist.gov/fips/fip180-1.txt]{Secure
            Hash Standard}, publi� en avril 1995. Il est disponible en
            ligne en texte simple (il manque au moins un diagramme), et 
            au format PDF � l'adresse
            \url{http://csrc.nist.gov/fips/fip180-1.pdf}.}
\end{seealso}

%T%% /TRANSLATION@


1.2
log
@Premi�re traduction
@
text
@d1 8
d12 6
d22 11
d41 1
a41 1
jusque l�.\index{checksum!SHA}  Les condens�s SHA sont longs de 160 bits,
d44 7
d57 5
d66 8
d80 7
d88 1
a88 1
  La taille du condens� en bytes. Vaut toujours \code{20}.
d91 4
d98 9
d115 8
d126 2
a127 2
20 caract�res qui peut contenir des caract�res non ASCII{}, y
compris des bytes � z�ro.
d130 9
d147 8
d161 13
d184 2
@


1.1
log
@Initial revision
@
text
@d2 1
a2 1
         SHA message digest algorithm}
d5 1
a5 1
\modulesynopsis{NIST's secure hash algorithm, SHA.}
d9 9
a17 8
This module implements the interface to NIST's\index{NIST} secure hash 
algorithm,\index{Secure Hash Algorithm} known as SHA.  It is used in
the same way as the \refmodule{md5} module:\ use \function{new()}
to create an sha object, then feed this object with arbitrary strings
using the \method{update()} method, and at any point you can ask it
for the \dfn{digest} of the concatenation of the strings fed to it
so far.\index{checksum!SHA}  SHA digests are 160 bits instead of
MD5's 128 bits.
d20 3
a22 3
\begin{funcdesc}{new}{\optional{string}}
  Return a new sha object.  If \var{string} is present, the method
  call \code{update(\var{string})} is made.
d26 2
a27 2
The following values are provided as constants in the module and as
attributes of the sha objects returned by \function{new()}:
d30 3
a32 3
  Size of the blocks fed into the hash function; this is always
  \code{1}.  This size is used to allow an arbitrary string to be
  hashed.
d36 1
a36 2
  The size of the resulting digest in bytes.  This is always
  \code{20}.
d40 1
a40 1
An sha object has the same methods as md5 objects:
d43 4
a46 3
Update the sha object with the string \var{arg}.  Repeated calls are
equivalent to a single call with the concatenation of all the
arguments, i.e.\ \code{m.update(a); m.update(b)} is equivalent to
d51 4
a54 3
Return the digest of the strings passed to the \method{update()}
method so far.  This is a 20-byte string which may contain
non-\ASCII{} characters, including null bytes.
d58 5
a62 4
Like \method{digest()} except the digest is returned as a string of
length 40, containing only hexadecimal digits.  This may 
be used to exchange the value safely in email or other non-binary
environments.
d66 3
a68 3
Return a copy (``clone'') of the sha object.  This can be used to
efficiently compute the digests of strings that share a common initial
substring.
d73 2
a74 2
            The Secure Hash Algorithm is defined by NIST document FIPS
            PUB 180-1:
d76 3
a78 3
            Hash Standard}, published in April of 1995.  It is
            available online as plain text (at least one diagram was
            omitted) and as PDF at
@


1.1.1.1
log
@Import version initiale de doc Python 2.0c1
@
text
@@
