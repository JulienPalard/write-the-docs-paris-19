head	1.1;
branch	1.1.1;
access;
symbols
	import-release_2_0_1:1.1.1.2
	IMPORT:1.1.1
	import-release20:1.1.1.2
	import-r20c1:1.1.1.1
	Import_Dist_Orig:1.1.1;
locks; strict;
comment	@% @;


1.1
date	2000.10.17.06.29.43;	author olberger;	state Exp;
branches
	1.1.1.1;
next	;

1.1.1.1
date	2000.10.17.06.29.43;	author olberger;	state Exp;
branches;
next	1.1.1.2;

1.1.1.2
date	2000.10.17.06.43.44;	author olberger;	state Exp;
branches;
next	;


desc
@@


1.1
log
@Initial revision
@
text
@\section{\module{readline} ---
         GNU readline interface}

\declaremodule{builtin}{readline}
  \platform{Unix}
\sectionauthor{Skip Montanaro}{skip@@mojam.com}
\modulesynopsis{GNU Readline in Python.}


The \module{readline} module defines a number of functions used either
directly or from the \refmodule{rlcompleter} module to facilitate
completion and history file read and write from the Python
interpreter.

The \module{readline} module defines the following functions:


\begin{funcdesc}{parse_and_bind}{string}
Parse and execute single line of a readline init file.
\end{funcdesc}

\begin{funcdesc}{get_line_buffer}{}
Return the current contents of the line buffer.
\end{funcdesc}

\begin{funcdesc}{insert_text}{string}
Insert text into the command line.
\end{funcdesc}

\begin{funcdesc}{read_init_file}{\optional{filename}}
Parse a readline initialization file.
The default filename is the last filename used.
\end{funcdesc}

\begin{funcdesc}{read_history_file}{\optional{filename}}
Load a readline history file.
The default filename is \file{\~{}/.history}.
\end{funcdesc}

\begin{funcdesc}{write_history_file}{\optional{filename}}
Save a readline history file.
The default filename is \file{\~{}/.history}.
\end{funcdesc}

\begin{funcdesc}{get_history_length}{}
Return the desired length of the history file.  Negative values imply
unlimited history file size.
\end{funcdesc}

\begin{funcdesc}{set_history_length}{length}
Set the number of lines to save in the history file.
\function{write_history_file()} uses this value to truncate the
history file when saving.  Negative values imply unlimited history
file size.
\end{funcdesc}

\begin{funcdesc}{set_completer}{\optional{function}}
Set or remove the completer function.  The completer function is
called as \code{\var{function}(\var{text}, \var{state})},
\code{for i in [0, 1, 2, ...]} until it returns a non-string.
It should return the next possible completion starting with \var{text}.
\end{funcdesc}

\begin{funcdesc}{get_begidx}{}
Get the beginning index of the readline tab-completion scope.
\end{funcdesc}

\begin{funcdesc}{get_endidx}{}
Get the ending index of the readline tab-completion scope.
\end{funcdesc}

\begin{funcdesc}{set_completer_delims}{string}
Set the readline word delimiters for tab-completion.
\end{funcdesc}

\begin{funcdesc}{get_completer_delims}{}
Get the readline word delimiters for tab-completion.
\end{funcdesc}


\begin{seealso}
  \seemodule{rlcompleter}{Completion of Python identifiers at the
                          interactive prompt.}
\end{seealso}


\subsection{Example \label{readline-example}}

The following example demonstrates how to use the
\module{readline} module's history reading and writing functions to
automatically load and save a history file named \file{.pyhist} from
the user's home directory.  The code below would normally be executed
automatically during interactive sessions from the user's
\envvar{PYTHONSTARTUP} file.

\begin{verbatim}
import os
histfile = os.path.join(os.environ["HOME"], ".pyhist")
try:
    readline.read_history_file(histfile)
except IOError:
    pass
import atexit
atexit.register(readline.write_history_file, histfile)
del os, histfile
\end{verbatim}
@


1.1.1.1
log
@Import version initiale de doc Python 2.0c1
@
text
@@


1.1.1.2
log
@Import version de doc Python 2.0
@
text
@d7 1
a7 1
\modulesynopsis{GNU readline support for Python.}
@

