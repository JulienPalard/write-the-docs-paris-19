head	1.2;
access;
symbols
	import-release_2_0_1:1.1.1.1
	IMPORT:1.1.1
	import-release20:1.1.1.1
	import-r20c1:1.1.1.1
	Import_Dist_Orig:1.1.1;
locks; strict;
comment	@% @;


1.2
date	2000.11.05.15.55.32;	author blacherez;	state Exp;
branches;
next	1.1;

1.1
date	2000.10.17.06.29.23;	author olberger;	state Exp;
branches
	1.1.1.1;
next	;

1.1.1.1
date	2000.10.17.06.29.23;	author olberger;	state Exp;
branches;
next	;


desc
@@


1.2
log
@Premi�re traduction
@
text
@\chapter{Introduction}
\label{intro}

%The ``Python library'' contains several different kinds of components.
La biblioth�que Python contient diff�rentes sortes de composants.

%It contains data types that would normally be considered part of the
%``core'' of a language, such as numbers and lists.  For these types,
%the Python language core defines the form of literals and places some
%constraints on their semantics, but does not fully define the
%semantics.  (On the other hand, the language core does define
%syntactic properties like the spelling and priorities of operators.)
Elle inclue des types de donn�es qui sont normalement int�gr�s au ``noyau''
d'un langage, comme les nombres et les listes. Pour ces types, le noyau de
Python d�finit la forme des litt�raux et pose certaines contraintes sur leur
s�mantique, mais ne d�finit pas enti�rement la s�mantique. (D'un autre c�t�,
le noyau du language Python d�finit des propri�t�s syntaxiques comme les
formules et les priorit�s des op�rateurs).

%The library also contains built-in functions and exceptions ---
%objects that can be used by all Python code without the need of an
%\keyword{import} statement.  Some of these are defined by the core
%language, but many are not essential for the core semantics and are
%only described here.
La biblioth�que contient aussi les fonctions int�gr�es et les exceptions ---
des objets qui peuvent �tre utilis�s par tout code Python sans recours � la
commande \keyword{import}. La plupart de ceux-ci sont d�finis par le noyau du
langage, mais beaucoup ne sont pas essentiels pour la s�mantique du noyau et
sont seulement d�crit ici.

%The bulk of the library, however, consists of a collection of modules.
%There are many ways to dissect this collection.  Some modules are
%written in C and built in to the Python interpreter; others are
%written in Python and imported in source form.  Some modules provide
%interfaces that are highly specific to Python, like printing a stack
%trace; some provide interfaces that are specific to particular
%operating systems, such as access to specific hardware; others provide
%interfaces that are
%specific to a particular application domain, like the World-Wide Web.
%Some modules are available in all versions and ports of Python; others
%are only available when the underlying system supports or requires
%them; yet others are available only when a particular configuration
%option was chosen at the time when Python was compiled and installed.
La majorit� de la biblioth�que, cependant, consiste en une collection de
modules. Il y a plusieurs fa�ons de diss�quer cette collection. Certains modules
sont �crits en C et int�gr�s dans l'interpr�teur Python ; les autres sont
�crits en Python et import�s sous forme de sources. Certains modules fournissent
des interfaces sp�cifiques � Python, comme imprimer un �tat de la pile ;
certains fournissent des interfaces propres � un certain syst�me d'exploitation;
d'autres fournissent des interfaces � un domaine particulier d'application,
comme le World Wide Web. Certains modules sont disponibles dans toutes les
versions et portages de Python ; d'autres ne sont disponibles que sur les
syst�mes qui les supportent ou les requi�rent ; enfin d'autre ne sont
disponibles que si certaines options de configuration ont �t�s choisies quand
Python a �t� compil� et intall�.

%This manual is organized ``from the inside out:'' it first describes
%the built-in data types, then the built-in functions and exceptions,
%and finally the modules, grouped in chapters of related modules.  The
%ordering of the chapters as well as the ordering of the modules within
%each chapter is roughly from most relevant to least important.
Ce manuel est organis� � l'envers : il d�crit d'abord les types de donn�es
int�gr�s, puis les fonctions int�gr�es et les exceptions, et finalement les
modules, group�s en chapitres de modules li�s. L'ordre des chapitres, comme
l'ordre des modules � l'int�rieur de chaque chapitre, est en gros du plus
pertinent au moins important.

%This means that if you start reading this manual from the start, and
%skip to the next chapter when you get bored, you will get a reasonable
%overview of the available modules and application areas that are
%supported by the Python library.  Of course, you don't \emph{have} to
%read it like a novel --- you can also browse the table of contents (in
%front of the manual), or look for a specific function, module or term
%in the index (in the back).  And finally, if you enjoy learning about
%random subjects, you choose a random page number (see module
%\refmodule{random}) and read a section or two.  Regardless of the
%order in which you read the sections of this manual, it helps to start 
%with chapter \ref{builtin}, ``Built-in Types, Exceptions and
%Functions,'' as the remainder of the manual assumes familiarity with
%this material.
Cela signifie que si vous commencez � lire le manuel du d�but et que vous passez
au chapitre suivant quand vous vous ennuyez, vous aurez une vue correcte des
modules disponibles et des sortes d'applications qui sont support�s par la
biblioth�que Python. Bien sur, vous \emph{n'avez pas} � lire ce manuel comme une
nouvelle --- vous pouvez aussi parcourir la table des mati�res (au d�but du
manuel), ou regarder une fonction sp�cifique, un module ou un terme dans l'index
(� la fin du manuel). Et finalement, si vous �tes simplement heureux d'�tudier
des sujets au hasard, choisissez une page en aveugle (regarder le module
\refmodule{random}) et lisez une section ou deux. Sans se soucier de l'ordre
dans lequel vous lisez les sections de ce manuel, cela aide de commencer par le
chapitre \ref{builtin}, ``Types int�gr�s, Exceptions et Fonctions'', car le
reste du manuel consid�re que vous �tes famili� avec ces notions.

%Let the show begin!
Que le spectacle commence !
@


1.1
log
@Initial revision
@
text
@d1 95
a95 53
\chapter{Introduction}
\label{intro}

The ``Python library'' contains several different kinds of components.

It contains data types that would normally be considered part of the
``core'' of a language, such as numbers and lists.  For these types,
the Python language core defines the form of literals and places some
constraints on their semantics, but does not fully define the
semantics.  (On the other hand, the language core does define
syntactic properties like the spelling and priorities of operators.)

The library also contains built-in functions and exceptions ---
objects that can be used by all Python code without the need of an
\keyword{import} statement.  Some of these are defined by the core
language, but many are not essential for the core semantics and are
only described here.

The bulk of the library, however, consists of a collection of modules.
There are many ways to dissect this collection.  Some modules are
written in C and built in to the Python interpreter; others are
written in Python and imported in source form.  Some modules provide
interfaces that are highly specific to Python, like printing a stack
trace; some provide interfaces that are specific to particular
operating systems, such as access to specific hardware; others provide
interfaces that are
specific to a particular application domain, like the World-Wide Web.
Some modules are available in all versions and ports of Python; others
are only available when the underlying system supports or requires
them; yet others are available only when a particular configuration
option was chosen at the time when Python was compiled and installed.

This manual is organized ``from the inside out:'' it first describes
the built-in data types, then the built-in functions and exceptions,
and finally the modules, grouped in chapters of related modules.  The
ordering of the chapters as well as the ordering of the modules within
each chapter is roughly from most relevant to least important.

This means that if you start reading this manual from the start, and
skip to the next chapter when you get bored, you will get a reasonable
overview of the available modules and application areas that are
supported by the Python library.  Of course, you don't \emph{have} to
read it like a novel --- you can also browse the table of contents (in
front of the manual), or look for a specific function, module or term
in the index (in the back).  And finally, if you enjoy learning about
random subjects, you choose a random page number (see module
\refmodule{random}) and read a section or two.  Regardless of the
order in which you read the sections of this manual, it helps to start 
with chapter \ref{builtin}, ``Built-in Types, Exceptions and
Functions,'' as the remainder of the manual assumes familiarity with
this material.

Let the show begin!
@


1.1.1.1
log
@Import version initiale de doc Python 2.0c1
@
text
@@
