head	1.4;
access;
symbols
	import-release_2_0_1:1.1.1.2
	IMPORT:1.1.1
	import-release20:1.1.1.2
	import-r20c1:1.1.1.1
	Import_Dist_Orig:1.1.1;
locks; strict;
comment	@% @;


1.4
date	2002.02.19.19.13.00;	author fgranger;	state Exp;
branches;
next	1.3;

1.3
date	2002.02.19.19.09.07;	author fgranger;	state Exp;
branches;
next	1.2;

1.2
date	2000.11.05.16.07.17;	author blacherez;	state Exp;
branches;
next	1.1;

1.1
date	2000.10.17.06.30.15;	author olberger;	state Exp;
branches
	1.1.1.1;
next	;

1.1.1.1
date	2000.10.17.06.30.15;	author olberger;	state Exp;
branches;
next	1.1.1.2;

1.1.1.2
date	2000.10.17.06.44.16;	author olberger;	state Exp;
branches;
next	;


desc
@@


1.4
log
@traduit et relu 20020219
@
text
@%%% TRANSLATION INFO :
% Original : release20
% Translation : $Id: libmac.tex,v 1.2 2002/02/19 19:09:07 fgranger Exp $
% Translator : Fran�ois Granger <fgranger@@mac.com>
% Corrector : Michel Bisiere <michel.bisiere@@free.fr> Pass 1 on 2002/02/18
%%% TRANSLATION ORIG
% \section{Introduction}
% \label{intro}
%%% TRANSLATION FR

\section{Introduction}
\label{intro}
%%% /TRANSLATION

%%% TRANSLATION ORIG
% The modules in this manual are available on the Apple Macintosh only.
%%% TRANSLATION FR
Les modules de ce manuel ne sont disponibles que pour le Macintosh d'Apple.
%%% /TRANSLATION

%%% TRANSLATION ORIG
% Aside from the modules described here there are also interfaces to
% various MacOS toolboxes, which are currently not extensively
% described. The toolboxes for which modules exist are:
% \module{AE} (Apple Events),
% \module{Cm} (Component Manager),
% \module{Ctl} (Control Manager),
% \module{Dlg} (Dialog Manager),
% \module{Evt} (Event Manager),
% \module{Fm} (Font Manager),
% \module{List} (List Manager),
% \module{Menu} (Moenu Manager),
% \module{Qd} (QuickDraw),
% \module{Qt} (QuickTime),
% \module{Res} (Resource Manager and Handles),
% \module{Scrap} (Scrap Manager),
% \module{Snd} (Sound Manager),
% \module{TE} (TextEdit),
% \module{Waste} (non-Apple \program{TextEdit} replacement) and
% \module{Win} (Window Manager).
%%% TRANSLATION FR
En dehors des modules d�crits ici, il existe �galement des modules d'interfaces pour les 
diff�rentes "ToolBox" d'Apple. Ils ne sont pas d�crits en d�tail ici. 
Les parties de la "ToolBox" pour lesquels il y existe des modules sont:
\module{AE} (Apple Events),
\module{Cm} (Component Manager),
\module{Ctl} (Control Manager),
\module{Dlg} (Dialog Manager),
\module{Evt} (Event Manager),
\module{Fm} (Font Manager),
\module{List} (List Manager),
\module{Menu} (Moenu Manager),
\module{Qd} (QuickDraw),
\module{Qt} (QuickTime),
\module{Res} (Resource Manager and Handles),
\module{Scrap} (Scrap Manager),
\module{Snd} (Sound Manager),
\module{TE} (TextEdit),
\module{Waste} (remplacement d'origine non-Apple de \program{TextEdit}) et
\module{Win} (Window Manager).
%%% /TRANSLATION

%%% TRANSLATION ORIG
% If applicable the module will define a number of Python objects for
% the various structures declared by the toolbox, and operations will be
% implemented as methods of the object. Other operations will be
% implemented as functions in the module. Not all operations possible in
% \C{} will also be possible in Python (callbacks are often a problem), and
% parameters will occasionally be different in Python (input and output
% buffers, especially). All methods and functions have a \code{__doc__}
% string describing their arguments and return values, and for
% additional description you are referred to \citetitle{Inside
% Macintosh} or similar works.
%%% TRANSLATION FR

Dans la mesure du possible, le module d�fini des objets pour les diff�rentes 
structures d�clar�es par la toolbox. Les commandes seront impl�ment�es sous 
forme de m�thodes de ces objets. D'autres commandes seront impl�ment�es sous 
forme de fonctions du module. Les op�rations disponibles en Python ne couvriront 
pas toutes les commandes disponibles en \C{} (les fonctions de rappel -callbacks- 
sont souvent un probl�me). Les param�tres en Python peuvent �tre diff�rents 
(en particulier les buffers d'entr�es-sorties). Toutes les m�thodes et fonctions 
poss�dent un \code{__doc__} d�crivant leur argument et leur 
valeurs de retour. Pour une description plus compl�te, veuillez vous reporter 
�  \citetitle{Inside Macintosh} ou des documents similaires.
%%% /TRANSLATION

%%% TRANSLATION ORIG
% The following modules are documented here:
%%% TRANSLATION FR

Les modules suivants sont document�s :
%%% /TRANSLATION

%%% TRANSLATION ORIG
% \localmoduletable
%%% TRANSLATION FR

\localmoduletable
%%% /TRANSLATION


%%% TRANSLATION ORIG
% \section{\module{mac} ---
%          Implementations for the \module{os} module}
%%% TRANSLATION FR


\section{\module{mac} ---
         Impl�mentation du module \module{os}}
%%% /TRANSLATION

%%% TRANSLATION ORIG
% \declaremodule{builtin}{mac}
%   \platform{Mac}
% \modulesynopsis{Implementations for the \module{os} module.}
%%% TRANSLATION FR

\declaremodule{builtin}{mac}
  \platform{Mac}
\modulesynopsis{Impl�mentation du module \module{os}.}
%%% /TRANSLATION


%%% TRANSLATION ORIG
% This module implements the operating system dependent functionality
% provided by the standard module \module{os}\refstmodindex{os}.  It is
% best accessed through the \module{os} module.
%%% TRANSLATION FR


Ce module impl�mente les fonctionnalit�s d�pendantes du syst�me d'exploitation 
fournies pas le module standard \module{os}\refstmodindex{os}. Il est pr�f�rable 
de l'utiliser au travers du module \module{os}.
%%% /TRANSLATION

%%% TRANSLATION ORIG
% The following functions are available in this module:
% \function{chdir()},
% \function{close()},
% \function{dup()},
% \function{fdopen()},
% \function{getcwd()},
% \function{lseek()},
% \function{listdir()},
% \function{mkdir()},
% \function{open()},
% \function{read()},
% \function{rename()},
% \function{rmdir()},
% \function{stat()},
% \function{sync()},
% \function{unlink()},
% \function{write()},
% as well as the exception \exception{error}. Note that the times
% returned by \function{stat()} are floating-point values, like all time
% values in MacPython.
%%% TRANSLATION FR

Les fonctions suivantes sont disponibles dans ce module:
\function{chdir()},
\function{close()},
\function{dup()},
\function{fdopen()},
\function{getcwd()},
\function{lseek()},
\function{listdir()},
\function{mkdir()},
\function{open()},
\function{read()},
\function{rename()},
\function{rmdir()},
\function{stat()},
\function{sync()},
\function{unlink()},
\function{write()},
On dispose aussi de l'exception \exception{error}. Veuillez noter que les temps 
retourn�s par \function{stat()} sont des valeurs en virgule flottante comme toutes les valeurs 
de temps de MacPython.
%%% /TRANSLATION

%%% TRANSLATION ORIG
% One additional function is available:
%%% TRANSLATION FR

Une fonction suppl�mentaire est disponible :
%%% /TRANSLATION

%%% TRANSLATION ORIG
% \begin{funcdesc}{xstat}{path}
%   This function returns the same information as \function{stat()}, but
%   with three additional values appended: the size of the resource fork
%   of the file and its 4-character creator and type.
% \end{funcdesc}
%%% TRANSLATION FR

\begin{funcdesc}{xstat}{path}
  Cette fonction retourne les m�mes informations que \function{stat()}, mais avec 
  trois valeurs suppl�mentaires � la fin: la taille du resource fork du fichier 
  ainsi que le type et le cr�ateur sous forme de code de 4 caract�res.
\end{funcdesc}
%%% /TRANSLATION


%%% TRANSLATION ORIG
% \section{\module{macpath} ---
%          MacOS path manipulation functions}
%%% TRANSLATION FR


\section{\module{macpath} ---
         Fonctions de manipulation de chemin de MacOS}
%%% /TRANSLATION

%%% TRANSLATION ORIG
% \declaremodule{standard}{macpath}
% % Could be labeled \platform{Mac}, but the module should work anywhere and
% % is distributed with the standard library.
% \modulesynopsis{MacOS path manipulation functions.}
%%% TRANSLATION FR

\declaremodule{standard}{macpath}
Pourrait �tre appel� \platform{Mac}, mais ce module devrait fonctionner partout et il est distribu� avec la biblioth�que standard.
\modulesynopsis{Fonctions de manipulation de chemin de MacOS.}
%%% /TRANSLATION


%%% TRANSLATION ORIG
% This module is the Macintosh implementation of the \module{os.path}
% module.  It is most portably accessed as
% \module{os.path}\refstmodindex{os.path}.  Refer to the
% \citetitle[../lib/lib.html]{Python Library Reference} for
% documentation of \module{os.path}.
%%% TRANSLATION FR

Ce module est l'impl�mentation Macintosh du module \module{os.path}
Il est pr�f�rable d'y acc�der par \module{os.path}\refstmodindex{os.path}. 
Reportez-vous � \citetitle[../lib/lib.html]{Python Library Reference} 
pour la documentation de \module{os.path}.
%%% /TRANSLATION

%%% TRANSLATION ORIG
% The following functions are available in this module:
% \function{normcase()},
% \function{normpath()},
% \function{isabs()},
% \function{join()},
% \function{split()},
% \function{isdir()},
% \function{isfile()},
% \function{walk()},
% \function{exists()}.
% For other functions available in \module{os.path} dummy counterparts
% are available.
%%% TRANSLATION FR

Les fonctions suivantes sont disponibles dans ce module:
\function{normcase()},
\function{normpath()},
\function{isabs()},
\function{join()},
\function{split()},
\function{isdir()},
\function{isfile()},
\function{walk()},
\function{exists()}.
Pour les autres fonctions disponibles dans \module{os.path} des fonctions vides 
correspondantes sont disponibles.
%%% /TRANSLATION
@


1.3
log
@traduit et relu 20020219
@
text
@d3 1
a3 1
% Translation : $Id: libmac.tex,v 1.2 2002/02/19 20:06:13 fgranger Exp $
@


1.2
log
@Premi�re traduction
@
text
@d3 3
a5 2
% Translation : $Id: libmac.tex,v 1.4 2000/10/17 13:20:50 fgranger Exp $
% Translator : Fran�ois Granger <fgranger@@altern.org>
a17 1

d42 3
a44 4

En dehors des modules d�crits ici, il y a des modules d'interfaces pour les 
diff�rentes "ToolBox" d'Apple. Ils ne sont pas d�crits en d�tail ici. 
Les modules de la "ToolBox" pour lesquels il y a des modules sont:
d76 9
a84 9
Dans la mesure du possible, le module d�fini des objets pour les diff�rentes 
structures d�clar�es par la toolbox. Les op�rations seront impl�ment�es sous 
forme de m�thodes de ces objets. D'autres op�rations seront impl�ment�es sous 
forme de fonctions du module. Les op�rations disponibles en Python ne couvriront 
pas toutes les op�ration disponibles en \C{} (les appels en retours -callbacks- 
sont souvent un probl�me). Les param�tres en Python peuvent �tre diff�rents 
(en particulier les buffers d'entr�es-sorties). Toutes les m�thodes et fonctions 
sont document�es par texte \code{__doc__} d�crivant leur argument et leur 
valeurs de retour. Pour une description plus compl�te, veuillez vous reporter 
d92 1
a92 1
Les modules suivants sont document�s :
d110 1
a110 1
         Impl�mentation du module \module{os}}
d121 1
a121 1
\modulesynopsis{Impl�mentation du module \module{os}.}
d132 2
a133 2
Ce module impl�mente les fonctionnalit�s d�pendantes du syst�me d'exploitation 
fournies pas le module standard \module{os}\refstmodindex{os}. Il est pr�f�rable 
d178 1
a178 1
retourn�s par \function{stat()} sont en virgule flottante comme toutes les valeurs 
d186 1
a186 1
Une fonction suppl�mentaire est disponible :
d198 3
a200 3
  Cette fonction retourne les m�mes informations que \function{stat()}, mais avec 
  trois valeurs suppl�mentaires � la fin: la taille du resource fork du fichier 
  ainsi que le type et le cr�ateur sous forme de code de 4 caract�res.
d223 1
a223 2
% Could be labeled \platform{Mac}, but the module should work anywhere and
% is distributed with the standard library.
d236 4
a239 5

Ce module est l'impl�mentation Macintosh du module \module{os.path}
Il est pr�f�rable d'y acc�der par \module{os.path}\refstmodindex{os.path}. 
Reportez-vous � \citetitle[../lib/lib.html]{Python Library Reference} pour 
la documentation de \module{os.path}.
d267 2
a268 1
Pour les autres fonctions disponibles dans \module{os.path} des fonctions vides correspondantes sont disponibles.
@


1.1
log
@Initial revision
@
text
@d1 9
d12 1
d14 32
a45 5
The modules in this manual are available on the Apple Macintosh only.

Aside from the modules described here there are also interfaces to
various MacOS toolboxes, which are currently not extensively
described. The toolboxes for which modules exist are:
d60 1
a60 1
\module{Waste} (non-Apple \program{TextEdit} replacement) and
d62 1
d64 35
a98 10
If applicable the module will define a number of Python objects for
the various structures declared by the toolbox, and operations will be
implemented as methods of the object. Other operations will be
implemented as functions in the module. Not all operations possible in
\C{} will also be possible in Python (callbacks are often a problem), and
parameters will occasionally be different in Python (input and output
buffers, especially). All methods and functions have a \code{__doc__}
string describing their arguments and return values, and for
additional description you are referred to \citetitle{Inside
Macintosh} or similar works.
d100 2
a101 1
The following modules are documented here:
d103 5
a107 1
\localmoduletable
d111 8
a118 1
         Implementations for the \module{os} module}
d122 2
a123 1
\modulesynopsis{Implementations for the \module{os} module.}
d126 34
a159 3
This module implements the operating system dependent functionality
provided by the standard module \module{os}\refstmodindex{os}.  It is
best accessed through the \module{os} module.
d161 1
a161 1
The following functions are available in this module:
d178 19
a196 5
as well as the exception \exception{error}. Note that the times
returned by \function{stat()} are floating-point values, like all time
values in MacPython.

One additional function is available:
d199 3
a201 3
  This function returns the same information as \function{stat()}, but
  with three additional values appended: the size of the resource fork
  of the file and its 4-character creator and type.
d203 7
d213 9
a221 1
         MacOS path manipulation functions}
d226 2
a227 1
\modulesynopsis{MacOS path manipulation functions.}
d230 29
a258 5
This module is the Macintosh implementation of the \module{os.path}
module.  It is most portably accessed as
\module{os.path}\refstmodindex{os.path}.  Refer to the
\citetitle[../lib/lib.html]{Python Library Reference} for
documentation of \module{os.path}.
d260 1
a260 1
The following functions are available in this module:
d270 2
a271 2
For other functions available in \module{os.path} dummy counterparts
are available.
@


1.1.1.1
log
@Import version initiale de doc Python 2.0c1
@
text
@@


1.1.1.2
log
@Import version de doc Python 2.0
@
text
@d1 41
@

