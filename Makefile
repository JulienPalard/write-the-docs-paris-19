SRCS := $(wildcard *.md)
HTML := $(SRCS:.md=.html)

.PHONY: static
static: $(HTML)

%.html: %.md
	mdtoreveal $< -o $@

.PHONY: clean
clean:
	rm -f *.html
